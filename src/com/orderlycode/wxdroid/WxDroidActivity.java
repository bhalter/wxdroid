//Copyright 2012, Bryan Halter (bhalter@armyofpenguins.com), all rights reserved 
package com.orderlycode.wxdroid;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class WxDroidActivity extends Activity {

Button submitButton;
EditText airportListString;
public static final String airportList = "com.orderlycode.wxdroid.airportListMessage";	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        //Boilerplate instantiation
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        final String lastAirportsPref = "lastAirports";
        submitButton = (Button) findViewById(R.id.checkWxButton);
        //Last query is stored as a preference, retrieve that and set it as the default
        SharedPreferences sp = getSharedPreferences("wxDroidPrefs",0);
        String lastAirports = sp.getString(lastAirportsPref, "");
        airportListString = (EditText)  findViewById(R.id.airportEntry);
        airportListString.setText(lastAirports);
        final SharedPreferences.Editor spEditor = sp.edit();
        OnClickListener ocl = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//Update last query for next time
				spEditor.putString(lastAirportsPref, airportListString.getText().toString());
				spEditor.commit();
				//Hand off airport list to next activity
				Intent wxCheckIntent = new Intent(WxDroidActivity.this, WxCheckActivity.class);
				wxCheckIntent.putExtra(airportList, airportListString.getText().toString());
				startActivity(wxCheckIntent);
			}
		};
        submitButton.setOnClickListener(ocl);   
    }
}
//Copyright 2012, Bryan Halter (bhalter@armyofpenguins.com), all rights reserved 

package com.orderlycode.wxdroid;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class Metar {
	String site;
	InputStream metar;
	StringBuffer rules;
	public static final String wxURL = "http://weather.aero/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&hoursBeforeNow=1&stationString=";
	public Metar (String site) 
	{
		this.site = site;
	}
	
	public String getSite()
	{
		return this.site;
	}
	
	
    public StringBuffer getFlightConditions () throws IOException
    {
    	//only need to do the expensive things if they haven't been done yet
    	if (this.rules == null)
    		refreshFlightConditions();
    	return this.rules;
    }
    public void refreshFlightConditions () throws IOException
    {
    	
    	StringBuffer rules = new StringBuffer();
    	try
    	{
    		DocumentBuilderFactory domFactory =	DocumentBuilderFactory.newInstance();
 		    domFactory.setNamespaceAware(true); 
    		DocumentBuilder builder = domFactory.newDocumentBuilder();
    		if (this.metar == null)
    			this.getMETAR();
    		Document doc = builder.parse(this.metar);
    		XPath xpath = XPathFactory.newInstance().newXPath();
    		XPathExpression expr = xpath.compile("/response/data/METAR/flight_category/text()");
    		Object result = expr.evaluate(doc, XPathConstants.NODESET);
    		NodeList nodes = (NodeList) result;
    		try
    		{
    			rules.append(nodes.item(0).getNodeValue());
    		}
    		catch (Exception e)
    		{
    			IOException ioe = new IOException("no data for site");
    			throw ioe;
    		}
    	}
    	catch (Exception e)
    	{
    		if (e instanceof java.io.IOException)
    			throw (IOException) e;
    		else
    			return;
    	}
    	this.rules = rules;
    }
    public void getMETAR () throws IOException, MalformedURLException 
    {
    	InputStream metarData;
    	URL url= new URL(wxURL+this.getSite());
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection(); 
	    metarData = conn.getInputStream();
	    if (conn.getResponseCode() != 200)	
	    {
	    	IOException e = new IOException();
	    	throw e;
	    }
	    this.metar = metarData;
	    	
    }

}

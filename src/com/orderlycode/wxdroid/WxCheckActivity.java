//Copyright 2012, Bryan Halter (bhalter@armyofpenguins.com), all rights reserved 

package com.orderlycode.wxdroid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class WxCheckActivity extends Activity{
		
	public void onCreate(Bundle savedInstanceState)
    {
		//Boilerplate activity creation
		super.onCreate(savedInstanceState);
		setContentView(R.layout.weather);
		Intent intent = getIntent();
		
		//Grab the airport list passed from last activity
		String message = intent.getStringExtra(com.orderlycode.wxdroid.WxDroidActivity.airportList);
    	TextView tv = (TextView)this.findViewById(R.id.wxBox);
        tv.setText("retrieving METAR info");
     	tv.setText("uninitialized");
    	//Get the list of airports pared down to decent data, initialize data structures for processing
		List <String> siteCode = AirportList.decode(message);
		List <StringBuffer> conditions = new ArrayList <StringBuffer>();
		StringBuffer output = new StringBuffer();
		StringBuffer metarOutput = new StringBuffer();
		Iterator <String> i = siteCode.listIterator();
		String site = new String();
		//For each site specified, get data or default to warning
		while (i.hasNext())
		{
			site = i.next();
			Metar metar = new Metar(site);
			try
			{
			metarOutput = metar.getFlightConditions();
			conditions.add(metarOutput);
			}
			catch (Exception e)
			{
				metarOutput = new StringBuffer("UNKNOWN-Report Missing");
			}
			output.append(site);
			output.append(":");
			output.append(metarOutput);
			if (i.hasNext())
				output.append("\n");
		}
		tv.setText(output);
		ColorMapper cm=new ColorMapper(conditions);
		//Set the text background color to the worse condition observed in the list
		tv.setBackgroundResource(cm.getConditionColor());     
    }    
}

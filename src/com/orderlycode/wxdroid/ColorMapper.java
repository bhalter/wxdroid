//Copyright 2012, Bryan Halter (bhalter@armyofpenguins.com), all rights reserved 

package com.orderlycode.wxdroid;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

public class ColorMapper {
	Collection<StringBuffer> condition;
	public ColorMapper (Collection<StringBuffer> conditions)
	{
		this.condition = conditions;
	}
	public int getConditionColor()
	{
		int conditionColor = R.color.VFR;
		for (StringBuffer siteCondition: this.condition)
		{
			if (siteCondition.toString().contentEquals("LIFR"))
				conditionColor = R.color.LIFR;
			else if (siteCondition.toString().contentEquals("IFR"))
				conditionColor = R.color.IFR;
			else if (siteCondition.toString().contentEquals("MVFR"))
				conditionColor = R.color.MVFR;
		}
		return conditionColor;
	}
	@Test public static void testForLIFR(){
		Collection<StringBuffer> FR = new ArrayList <StringBuffer>() ;
		FR.add(new StringBuffer("LIFR"));
		FR.add(new StringBuffer("IFR"));
		FR.add(new StringBuffer("MVFR"));
		FR.add(new StringBuffer("VFR"));
		ColorMapper colmap = new ColorMapper(FR);
		int expected=colmap.getConditionColor();
		assertTrue(expected == R.color.LIFR);
	}
	@Test public static void testForIFR(){
		Collection<StringBuffer> FR = new ArrayList <StringBuffer>() ;
		FR.add(new StringBuffer("IFR"));
		FR.add(new StringBuffer("MVFR"));
		FR.add(new StringBuffer("VFR"));
		ColorMapper colmap = new ColorMapper(FR);
		int expected=colmap.getConditionColor();
		assertTrue(expected == R.color.IFR);
	}
	@Test public static void testForMVFR(){
		Collection<StringBuffer> FR = new ArrayList <StringBuffer>() ;
		FR.add(new StringBuffer("MVFR"));
		FR.add(new StringBuffer("VFR"));
		ColorMapper colmap = new ColorMapper(FR);
		int expected=colmap.getConditionColor();
		assertTrue(expected == R.color.MVFR);
	}
	@Test public static void testForVFR(){
		Collection<StringBuffer> FR = new ArrayList <StringBuffer>() ;
		FR.add(new StringBuffer("VFR"));
		ColorMapper colmap = new ColorMapper(FR);
		int expected=colmap.getConditionColor();
		assertTrue(expected == R.color.VFR);
	}

}
	
	


//Copyright 2012, Bryan Halter (bhalter@armyofpenguins.com), all rights reserved 

package com.orderlycode.wxdroid;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class AirportList {

public static List<String> decode (String airports)
{
	String[] airportArray;
	Pattern p = Pattern.compile("[ ]+");
	airportArray = p.split(airports);
	List<String> airportList = new ArrayList<String>();
	for (int i=0; i < airportArray.length; i++)
		airportList.add(airportArray[i]);
	return (List<String>) airportList;
}

}
